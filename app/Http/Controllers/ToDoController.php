<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Psr7\Response;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ToDoController extends Controller
{
    public function index()
    {
        $client = new Client();
        $response = $client->get('http://127.0.0.1:3000/api/employees');
        $json = json_decode($response->getBody(), true); 
        return view('admin/index',compact('json'));
    }
    
    public function show($id)
    {
        $client = new Client();
        $response = $client->get('http://127.0.0.1:3000/api/employee/'.$id);
        $json = json_decode($response->getBody(), true); 
        return view('admin/show',compact('json'));
    }

    public function create()
    {
        return view('admin/create');
    }

    public function store(Request $req)
    {  
        $client = new Client();
        $response = $client->request('POST', "http://127.0.0.1:3000/api/addEmployee", [
            'json' => [
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'salary' => $req->input('salary')
            ]
        ]);

        return redirect('todos')->with('success', 'Information has been added');
    }

    public function edit($id, Request $req)
    {
        $client = new Client();
        $response = $client->get('http://127.0.0.1:3000/api/employee/'.$id);
        $json = json_decode($response->getBody(), true); 
        return view('admin/edit',compact('json'));
    }
    public function update(Request $req, $id)
    {
        $client = new Client();
        $response = $client->request('PUT', 'http://127.0.0.1:3000/api/updateEmployee/'.$id, [
            'json' => [
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'salary'=> $req->input('salary')
            ]
        ]);

        return redirect('todos')->with('success', 'Information has been updated');
    }
    public function destroy($id)
    {
        $client = new Client();
        $response = $client->request('DELETE', 'http://127.0.0.1:3000/api/deleteEmployee/'.$id);
        return redirect('todos')->with('success', 'Information has been deleted');
    }
}

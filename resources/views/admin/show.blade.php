@extends('layouts.main')

@section('title', 'Show')

@section('content')

<div class="container">
<h2>Show A Form</h2><br/>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Id:</label>
                <input type="text" class="form-control" name="id" value="{{$json['id']}}" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{$json['name']}}" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value="{{$json['email']}}" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="salary">Salary:</label>
                <input type="text" class="form-control" name="salary" value="{{$json['salary']}}" disabled>
            </div>
        </div>

</div>

@endsection
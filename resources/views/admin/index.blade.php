@extends('layouts.main')

@section('title', 'Index')

@section('content')

<div class="container">
    <h2>Hover Rows</h2>    
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br/>
        @endif
    <a href="{{action('ToDoController@create')}}"><input type="button" class="btn btn-success" value="Create"></a>    
    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Salary</th>
                <th>Action</th>
            </tr>
        </thead>
        @foreach($json as $data)
        <tbody>
            <tr>
                <td>{{$data['id']}}</td>
                <td>{{$data['name']}}</td>
                <td>{{$data['email']}}</td>
                <td>{{$data['salary']}}</td>
                <td>
                    <a href="{{action('ToDoController@show', $data['id'])}}"><input type="button" class="btn btn-primary" value="Show"></a>
                    <a href="{{action('ToDoController@edit', $data['id'])}}"><input type="button" class="btn btn-warning" value="Edit"></a>
                    <form action="{{action('ToDoController@destroy', $data['id'])}}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
        @endforeach 
    </table>
</div>

@endsection
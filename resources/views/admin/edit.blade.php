@extends('layouts.main')

@section('title', 'Edit')

@section('content')

<div class="container">
    <form method="post" action="{{action('ToDoController@update',$json['id'])}}">
        @csrf
        <input name="_method" type="hidden" value="PUT">
        <h2>Edit A Form</h2><br/>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$json['name']}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email" value="{{$json['email']}}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="salary">salary:</label>
                    <input type="text" class="form-control" name="salary" value="{{$json['salary']}}">
                </div>
            </div>            
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
    </form>
</div>


@endsection
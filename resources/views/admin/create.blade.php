@extends('layouts.main')

@section('title', 'Create')

@section('content')

<div class="container">
    <form method="post" action="{{action('ToDoController@store')}}">
        @csrf
        <h2>Create A Form</h2><br/>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" name="email" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="salary">salary:</label>
                    <input type="text" class="form-control" name="salary" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
    </form>
</div>


@endsection